<?php

namespace App\CryptoServices;
use App\CryptoServices\ResponseCoinService;

abstract class BaseService implements BaseServiceInterface
{
    protected $address;
    
    protected $coin;
    
    public function __construct($address, $coin = null) {
        $this->address = $address;
        $this->coin = $coin;
    }
    
    abstract public function get();
        
    final public function response() {
        $response = $this->get();
        if (is_object($response) && ($response instanceof ResponseCoinService)) {
            return $response->getResponse();
        }
        return false;
    }
}