<?php

namespace App\CryptoServices;

Interface BaseServiceInterface
{
    public function response();
}