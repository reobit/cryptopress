<?php

namespace App\CryptoServices;
use GuzzleHttp\Client as HttpClient;
use App\CryptoServices\ResponseCoinService;

Class BlockExplorerComService extends BaseService
{
    public function get() {
        $response = new ResponseCoinService();
        
        try {
            $client = new HttpClient();
            
            $res = $client->request('GET', "https://blockexplorer.com/api/addr/{$this->address}/balance");

            $body = $res->getBody()->getContents();           
         
            $response->total = $body / 100000000;

        } catch(\GuzzleHttp\Exception\RequestException $ex) {
            $response->error   = true;
            $response->message = $ex->getResponse()->getBody()->getContents();
        } 
        return $response;
    }
    
}