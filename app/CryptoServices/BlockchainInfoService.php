<?php

namespace App\CryptoServices;
use GuzzleHttp\Client as HttpClient;
use App\CryptoServices\ResponseCoinService;

Class BlockchainInfoService extends BaseService
{
    public function get() {
        $response = new ResponseCoinService();
        
        try {
            $client = new HttpClient();
            
            $res = $client->request('GET', 'https://blockchain.info/ru/rawaddr/' . $this->address . '?limit=5');
            
            $body = $res->getBody()->getContents();
            
            $data = json_decode($body);
            
            $response->total = $data->final_balance / 100000000;
            
            $transactions = array();
            if ($data->txs) {
                foreach ($data->txs as $tx) {
                    $value = 0;
                    foreach ($tx->out as $out) {
                        $value += $out->value;
                        break;
                    }
                    $transactions[] = [
                        'value' => $value / 100000000,
                        'time' => $tx->time * 1000
                    ];
                }
            }
            
            $response->transactions = $transactions;
        } catch(\GuzzleHttp\Exception\ServerException $ex) {
            $response->error   = true;
            $response->message = $ex->getResponse()->getBody()->getContents();
        } 
        return $response;
    }
    
}