<?php

namespace App\CryptoServices;
use GuzzleHttp\Client as HttpClient;
use App\CryptoServices\ResponseCoinService;

Class EtherscanIoService extends BaseService
{
    public function get() {
        $response = new ResponseCoinService();
        
        try {
            $client = new HttpClient();
            
            $res = $client->request('GET', "https://api.etherscan.io/api?module=account&action=balance&address={$this->address}&tag=latest");

            $body = $res->getBody()->getContents();
            
            $data = json_decode($body);
         
            $response->total = $data->result;

        } catch(\GuzzleHttp\Exception\RequestException $ex) {
            $response->error   = true;
            $response->message = $ex->getResponse()->getBody()->getContents();
        } 
        return $response;
    }
    
}