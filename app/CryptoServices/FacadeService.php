<?php
namespace App\CryptoServices;

class FacadeService {
    
    public function __construct($config) {
        $this->config = $config;
    }
    
    static public function init($config) {
        return new self($config);
    }
    
    public function getIds() {
        $servicesKeys = array();
        foreach ($this->config as $group) {
            foreach ($group as $service) {
                $servicesKeys[] = $service['id'];
            }
        }
        return $servicesKeys;
    }
    
    public function getIdsByGroup() {
        $servicesKeys = array();
        foreach ($this->config as $groupId => $group) {
            foreach ($group as $service) {
                $servicesKeys[$groupId][] = [
                    'id' => $service['id'],
                    'name' => $service['name']
                ];
            }
        }
        return $servicesKeys;
    }
}

