<?php
namespace App\CryptoServices;
use App\CryptoServices\BaseServiceInterface;

Class FactoryService {
    
    protected $config;
    
    protected $coin;
    
    protected $address;
    
    public function __construct($config) {
        $this->config = $config;
    }
    
    static public function init($config) {
        return new self($config);
    }
    
    public function getServiceClassById($id) {
        foreach ($this->config as $group) {
            foreach ($group as $service) {
                if ($service['id'] == $id) {
                    return $service['class'];
                }
            }
        }
        return false;
    }
    
    public function getDefaultClassService() {
        if (isset($this->config[$this->coin])) {
            foreach ($this->config[$this->coin] as $service) {
                if (isset($service['default']) && $service['default']) {
                    return $service['class'];
                }
            }
        }
        return false;
    }
    
    public function create(\StdClass $params) {
        try {
            if (empty($params->coin) || empty($params->address)) {
                throw new \Exception('Coin or Address must be not empty');
            }
            
            $this->coin = $params->coin;
            $this->address = $params->address;
        
            if ($params->service) {
                $class = $this->getServiceClassById($params->service);
            } elseif($params) {
                $class = $this->getDefaultClassService($params);
            }
         
            if (class_exists($class) && ($obj = new $class($this->address, $this->coin)) && ($obj instanceof BaseServiceInterface)) {
                return $obj;
            }
            
            throw new \Exception('Service not avaliable');
            
        } catch(\Exception $ex) {
            return $ex->getMessage();
        }
    }
}

