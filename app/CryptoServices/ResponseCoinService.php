<?php
namespace App\CryptoServices;

class ResponseCoinService {
    public $total;
    
    public $error = false;
    
    public $message = "";
    
    public $transactions = array();

    public function getResponse() {
        if (!(bool)$this->error) {
            return [
                'success' => true,
                'total'   => $this->total,
                'transactions' => $this->transactions
            ];
        }
        return [
            'success' => false,
            'message' => $this->message
        ];
    }
}
