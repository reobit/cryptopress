<?php

namespace App\Helpers;

class WpHelper {

    protected $app;

    /**
     * Add Admin Notice
     * @param $app \Illuminate\Contracts\Foundation\Application
     */
    public function __construct($app) {
        $this->app = $app;
    }

    /**
     * Add Shortcode
     * @param string $tag
     * @param string $class
     * @return self
     */
    public function addShortcode($tag, \Closure $closure) {
        add_shortcode($tag, $closure);
        return $this;
    }

    /**
     * Add Widget
     * @param string $class
     * @return self
     */
    public function addWidget($class) {

        add_action('widgets_init', function() use ($class) {
            register_widget($this->app->build($class));
        });
        return $this;
    }

    /**
     * Enqueue Style
     * @param string $handle
     * @param string $src
     * @param array $dependencies
     * @param string $version
     * @param string $media [all|screen|print]
     * @return self
     */
    public function enqueueStyle($handle, $src, $dependencies = array(), $version = '1.0.0', $media = 'all') {

        add_action('wp_enqueue_scripts', function() use ($handle, $src, $dependencies, $version, $media) {
            wp_enqueue_style($handle, $src, $dependencies, $version, $media);
        });
        return $this;
    }

    /**
     * Enqueue Script
     * @param string $handle
     * @param string $src
     * @param array $dependencies
     * @param string $version
     * @param boolean $inFooter
     * @return self
     */
    public function enqueueScript($handle, $src, $dependencies = array(), $version = '1.0.0', $inFooter = true) {

        add_action('wp_enqueue_scripts', function() use ($handle, $src, $dependencies, $version, $inFooter) {
            wp_enqueue_script($handle, $src, $dependencies, $version, $inFooter);
        });
        return $this;
    }

    /**
     * Get Wp Global Post Object
     * @return object
     */
    public function getGlobalPost() {
        global $post;
        return $post;
    }

    /**
     * Get WP Database
     * @return array
     */
    public static function getWpDatabaseConnection() {

        global $wpdb;
        return [
            'driver' => 'mysql',
            'host' => DB_HOST,
            'database' => DB_NAME,
            'username' => DB_USER,
            'password' => DB_PASSWORD,
            'charset' => $wpdb->charset,
            'collation' => $wpdb->collate,
            'prefix' => $wpdb->prefix,
            'timezone' => '+00:00',
            'strict' => false,
        ];
    }

}
