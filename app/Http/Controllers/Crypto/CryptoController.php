<?php

namespace App\Http\Controllers\Crypto;

use App\Helpers\LumenHelper;
use App\Http\Controllers\Controller;
use App\Models\WpUser;
use App\CryptoServices\FactoryService;
use App\CryptoServices\FacadeService;

class CryptoController extends Controller {

    /**
     * Create a new controller instance.
     * @var $helper LumenHelper
     * @var $user WpUser
     */
    public function __construct(LumenHelper $helper, WpUser $user) {
        $this->helper = $helper;
        $this->request = $this->helper->request();
      
        $this->cryptoService = FactoryService::init(config('cryptoservices'));
        $this->cryptoFacade  = FacadeService::init(config('cryptoservices'));
        
    }
    
    public function index() {
        return $this->helper->response($this->cryptoFacade->getIds(), 200);
    }
    
    public function get() {
        $route = $this->request->route();
        $params = isset($route[2]) ? $route[2] : [];
        
        $service = $this->cryptoService->create((object)$params);
        
        if (is_object($service)) {
            return $this->helper->response($service->response(), 200);
        }
        
        return $service;
    }
    
    public function template() {
        return $this->helper->view('crypto.crypto', array(
            'user' => $this->request->user(),
            'services' => $this->cryptoFacade->getIdsByGroup()
        ));
    }

}
