<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class WordpressServiceProvider extends ServiceProvider {
    /** @var \App\Helpers\LumenHelper $lumenHelper * */

    /** @var \App\Helpers\WpHelper $wpHelper * */
    private $wpHelper, $lumenHelper, $absolutePath;

    /**
     * WordpressServiceProvider constructor.
     * @param $app \Illuminate\Contracts\Foundation\Application
     */
    public function __construct($app) {
        parent::__construct($app);
        $this->lumenHelper = $this->app->make('lumenHelper');
        $this->wpHelper = $this->lumenHelper->wpHelper();
    }

    /**
     * Register any application services.
     * @return void
     */
    public function register() {


        $this->wpHelper
            ->addWidget( \App\Widgets\CryptopressWidget::class );


        $this->wpHelper
            ->addShortcode(
                'cryptopress', function ($parameters, $content) {
                return $this->app->call('\App\Http\Controllers\Crypto\CryptoController@template', compact('parameters', 'content'));
            }
        );

        $this->wpHelper
            ->enqueueStyle(
                'cryptopress', $this->lumenHelper->asset('resources/assets/build/crypto.css'), array(), '1.0.0', 'all'
            )
            ->enqueueScript(
                'cryptopress', $this->lumenHelper->asset('resources/assets/build/crypto.js'), array('jquery'), '1.0.0', true
        );
    }

    public function boot() {
        
    }

}
