<?php

namespace App\Widgets;

use App\Helpers\LumenHelper;
use App\Models\WpPost;

class CryptopressWidget extends \WP_Widget {

    protected $helper, $post, $cache;

    function __construct(LumenHelper $helper, WpPost $post) {
        $this->helper = $helper;
        $this->post = $post;
        $this->cache = $this->helper->cache();
        parent::__construct(
            'cryptopress_widget', 'Cryptoporess Widget', array(
                'description' => 'List of cryptocoins',
            )
        );
    }

    public function form($instance) {
    }

    public function widget($args, $instance) {
        echo $this->helper->view('widgets.widget');
    }

    public function update($new_instance, $old_instance) {
        return $new_instance;
    }

}
