<?php

$wpLoad = realpath(__DIR__ . "/../../../../wp-load.php");
if (!class_exists('Laravel\Lumen\Application') && is_file($wpLoad)) {
    require_once($wpLoad);
} else {
    exit('Crypropress: wp-load.php not found.  Check path in bootstrap/app.php' . PHP_EOL);
}

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Laravel\Lumen\Application(
    realpath(__DIR__ . '/../')
);
$app->withEloquent();

$app->withFacades(true);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class, App\Exceptions\Handler::class
);
$app->singleton(
    Illuminate\Contracts\Console\Kernel::class, App\Console\Kernel::class
);

$app->routeMiddleware([
    'auth' => App\Http\Middleware\Authenticate::class,
    'start_session' => \Illuminate\Session\Middleware\StartSession::class,
    'share_errors' => \Illuminate\View\Middleware\ShareErrorsFromSession::class,
    'no404s' => App\Http\Middleware\SilenceWp404s::class
]);

$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\AuthServiceProvider::class);

if (!$app->runningInConsole()) {
    if ($app->make('config')->get('session.enabled')) {
        $app->bind(\Illuminate\Session\SessionManager::class, function ($app) {
            return new \Illuminate\Session\SessionManager($app);
        });
        $app->register(\Illuminate\Session\SessionServiceProvider::class);
        $app->middleware([\Illuminate\Session\Middleware\StartSession::class]);
    }
    
    $app->register(App\Providers\WordpressServiceProvider::class);
}

if (!$app->runningInConsole()) {
    add_action('init', function() use ( $app ) {
        $request = Illuminate\Http\Request::capture();
        if (!is_admin()) {

            $app->router->group([
                'namespace' => 'App\Http\Controllers',
                    ], function ( $router ) {
                require __DIR__ . '/../routes/web.php';
            });

            $response = $app->handle($request);

            if ($app->make('config')->get('router.loading') == 'eager') {

                if ($response->content()) {
                    $response->send();
                    exit($response->status());
                }
            } elseif (is_404()) {

                add_action('template_redirect', function() use ( $app, $request, $response ) {
                    if ($response->content()) {
                        $response->send();
                        exit($response->status());
                    }
                }, 1);
            }
        } else {
            $app->handle($request);
        }
    });
}
return $app;
