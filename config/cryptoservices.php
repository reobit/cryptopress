<?php

return [
    'bitcoin' => [
        [
            'id' => 'blockchain.info',
            'name' => 'blockchain.info',
            'class' => '\App\CryptoServices\BlockchainInfoService',
        ],
        [
            'id' => 'blockexplorer.com',
            'name' => 'blockexplorer.com',
            'class' => '\App\CryptoServices\BlockExplorerComService',
            'default' => true
        ]        
    ],
    'ethereum' => [
        [
            'id' => 'etherscan.io',
            'name' => 'etherscan.io',
            'class' => '\App\CryptoServices\EtherscanIoService',
            'default' => true
        ]
    ]  
];
