<?php
/**
 * @link              
 * @since             1.0
 * @package           cryptopress
 * @wordpress-plugin
 *
 * Plugin Name:       CryptoPress
 * Plugin URI:        
 * Description:        
 * Version:           1.0
 * Author:            Kirill
 * Author URI:        vk.com/kirillreo
 */

$app = require __DIR__ . '/bootstrap/app.php';
error_reporting((env('APP_DEBUG') ? E_ALL : 0));