import Vue from 'vue'
import VueSelect  from 'vue-select'
Vue.component('v-select', VueSelect);

Vue.use(require('vue-moment'));

let AppContainer = document.getElementById("root-crypto");

if (AppContainer !== null) {
    new Vue({
        components: {
            'crypto-service': Vue.component('crypto-service', require('./components/crypto.vue'))
        },
        data() {
            return {}
        },
        methods: {}
    }).$mount(AppContainer);
}