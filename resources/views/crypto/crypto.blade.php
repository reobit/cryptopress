<div id="root-crypto">
    <crypto-service :user="{{ json_encode($user) }}" :services="{{ json_encode($services) }}"></crypto-service>
</div>