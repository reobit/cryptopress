<?php

$router->group([
    'prefix' => 'cryptopress/api', 'middleware' => []
], function($router){
    $router->get('services', 'Crypto\CryptoController@index');
    $router->get('service/balance/{coin}/{address}', 'Crypto\CryptoController@get');
    $router->get('service/{service}/balance/{coin}/{address}', 'Crypto\CryptoController@get');
});