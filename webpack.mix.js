let mix = require('laravel-mix');

mix
    .js('resources/assets/src/crypto.js', 'build')
    .sass('resources/assets/src/crypto.sass', 'build')
    .setPublicPath('resources/assets')
    .sourceMaps(); 

